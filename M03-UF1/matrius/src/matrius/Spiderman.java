package matrius;

import java.util.Scanner;

public class Spiderman {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in); // Crea un objeto Scanner para la entrada de datos.



        // Lee las dimensiones de la matriz

        int f = scanner.nextInt(); // Lee el número de filas de la matriz.

        int c = scanner.nextInt(); // Lee el número de columnas de la matriz.



        // Lee la matriz

        String[][] matrix = new String[f][c]; // Crea una matriz para almacenar la información.

        for (int i = 0; i < f; i++) {

            for (int j = 0; j < c; j++) {

                matrix[i][j] = scanner.next(); // Lee y almacena cada elemento de la matriz.

            }

        }



        // Busca la posición de SPIDERMAN

        int spidermanRow = -1;

        int spidermanCol = -1;

        for (int i = 0; i < f; i++) {

            for (int j = 0; j < c; j++) {

                if (matrix[i][j].equals("SPIDERMAN")) { // Encuentra la posición de SPIDERMAN en la matriz.

                    spidermanRow = i;

                    spidermanCol = j;

                    break;

                }

            }

            if (spidermanRow != -1) {

                break;

            }

        }



        // Determina la clase que se perderá

        String result = "NO";

        if (spidermanRow > 0) {

            result = matrix[spidermanRow - 1][spidermanCol]; // Almacena la clase inmediatamente superior a SPIDERMAN.

        }



        // Imprime el resultado

        System.out.println(result);



        scanner.close(); // Cierra el objeto Scanner para liberar recursos.

 
	}

}
