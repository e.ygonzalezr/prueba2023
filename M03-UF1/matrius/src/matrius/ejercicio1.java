package matrius;

import java.util.Scanner;

public class ejercicio1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 Scanner src = new Scanner(System.in);
		 
		 
		 //tenim la grandaria de la matriu
		 
		final int FILES = src.nextInt();

		final int COLS = src.nextInt();
		
		//Omplim la matriu de valors
		int [][] mat = new int[FILES][COLS];
		 
		 for(int i= 0; i < FILES; i++) { //Per cada fila
			
			 for(int j = 0; j < COLS; j++) { //Per cada casella dins de la fila
				 mat[i][j]= src.nextInt();
			 }
		 }
		 
		 //Llegim la posició de la casella a consultar
		 int ii= src.nextInt();
		 int jj= src.nextInt();
		 
		 //Mostrem el contingut de la matriu
		 for(int i= 0; i < FILES; i++) { //Per cada fila
				
			 for(int j = 0; j < COLS; j++) { //Per cada casella dins de la fila
				 System.out.print(mat[i][j] + " ");
				 
			 }
			 System.out.println();
		 }
		 //Mostrem el valor guardat en(ii,jj)
		 System.out.println(mat[ii][jj]);
		 
		 
		 
		 
	}

}
