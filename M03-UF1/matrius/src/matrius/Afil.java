package matrius;

import java.util.Scanner;

public class Afil {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	
	
		        Scanner scanner = new Scanner(System.in);



		        // Llegeix el nombre de casos de prova

		        int casos = scanner.nextInt();



		        // Processa cada cas de prova

		        for (int i = 0; i < casos; i++) {

		            // Llegeix la posició de l'alfil

		            String posicio = scanner.next();

		            

		            // Obté la columna i fila de la posició

		            char columna = posicio.charAt(0);

		            int fila = Character.getNumericValue(posicio.charAt(1));



		            // Inicialitza la matriu 2x2 amb els moviments possibles de l'alfil

		            int[][] mat = {

		                {Math.min(8 - fila, fila - 1), Math.min(8 - columna + 1, columna - 'a')},

		                {Math.min(fila, 8 - fila + 1), Math.min(columna - 'a', 8 - columna + 1)}

		            };



		            // Calcula el nombre total de caselles a les quals es pot moure l'alfil

		            int moviments = 0;

		            for (int x = 0; x < 2; x++) {

		                for (int y = 0; y < 2; y++) {

		                    moviments += mat[x][y];

		                }

		            }



		            // Imprimeix la sortida per a aquest cas de prova

		            System.out.println(moviments);

		        }



		        scanner.close();


	
	}
	

}
