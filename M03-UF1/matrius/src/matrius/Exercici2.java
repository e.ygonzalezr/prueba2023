package matrius;

import java.util.Scanner;

public class Exercici2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 Scanner src = new Scanner(System.in);
		 
		 //tenim la grandaria
			
			final int FILES = src.nextInt();
			final int COLS = src.nextInt();
			
			int [][] mat = new int[FILES][COLS];
			
			//Omplim la matriu de valors
			for (int i = 0; i < FILES; i++) {  //Per cada fila
				
				for (int j = 0; j <COLS; j++) {	//Per cada casella dins de la fila
					
					mat[i][j] = src.nextInt();
				}
			}
			
			//Llegim la posició de la casella a consultar
			int v1 = src.nextInt();
			int v2 = src.nextInt();
			
			//Canviem tots els valors que siguin v1 per V2
			for (int i = 0; i < FILES; i++) {  //Per cada fila
				
				for (int j = 0; j <COLS; j++) {	//Per cada casella dins de la fila
					
					if (mat[i][j] == v1) mat[i][j] = v2;
				}
			}
			
			//Mostrem el contingut de la matriu
			for (int i = 0; i < FILES; i++) {  //Per cada fila
				
				for (int j = 0; j <COLS; j++) {	//Per cada casella dins de la fila
					
					System.out.print(mat[i][j] + " ");
				}
				System.out.println();
			}

	
		 
	}

}
