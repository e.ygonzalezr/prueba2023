package matrius;

import java.util.Scanner;

public class IndexofMatriu {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		final int F = src.nextInt();
		final int C = src.nextInt();

		int[][] mat = new int[F][C];
				
		// omplim les matrius de dades
		for (int i = 0; i < F; i++) { // Files
			for (int j = 0; j < C; j++) { // Columnes
				mat[i][j] = src.nextInt();
			}
		}
				
				
		int i = 0;
		int guardI = -1;
		int guardJ = -1;
		int k = src.nextInt();
		
		boolean trobat = false;
		
		while (i<F && trobat == false) {
			int j = 0;
				while (j<C && trobat == false) {
					if (mat[i][j] == k) {
						
						guardI = i;
						guardJ = j;
						
						trobat = true;
					}
					j++;
				 }
				i++;
			}
		System.out.println(guardI + " " + guardJ);
		src.close();

	}

}
