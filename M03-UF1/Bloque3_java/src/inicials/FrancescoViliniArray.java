package inicials;

import java.util.ArrayList;
import java.util.Scanner;

public class FrancescoViliniArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner src = new Scanner(System.in);
		
		for (int casos = src.nextInt(); casos > 0; casos--) {
			int k = src.nextInt();
			src.nextLine();
			ArrayList<String> llista = new ArrayList<String>();
			
			for (int pos = 0; pos < k; pos++) {
				llista.add(src.nextLine());
			}
			
			if (llista.contains("Francesco Virgolini") == true) {
				int pos = llista.indexOf("Francesco Virgolini");
				String guard = llista.get(pos-1);
				llista.set(pos-1, guard);
				llista.set(pos, guard);	
			}
			
			//System.out.println(llista);
			System.out.print("[");
			for (int p =0; p < llista.size()-1; p++) {
				System.out.print(llista.get(p) + ", ");
			}
			System.out.println(llista.get(k-1)+ "]");
		}	

	}

}
