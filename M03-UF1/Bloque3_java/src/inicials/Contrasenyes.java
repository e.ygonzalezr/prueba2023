package inicials;

import java.util.ArrayList;
import java.util.Scanner;

public class Contrasenyes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner sc = new Scanner(System.in);
		
		ArrayList<String> users = new ArrayList<String>();
		ArrayList<String> passwords = new ArrayList<String>();
		String uname, pwd;
		
		int arrayLength = sc.nextInt();
		sc.nextLine(); //Netejem buffer
		
		for(int i = 0; i < arrayLength; i++) {
			users.add(sc.next());	
		}
		//Demanem els passwords
		for(int i = 0; i < arrayLength; i++) {
			passwords.add(sc.next());
		}
		
		int numCasos = sc.nextInt();
		sc.nextLine(); //Netejem buffer
		
		for(int i = 0; i < numCasos; i++) {
			uname = sc.next();
			pwd = sc.next();
			
			if(!users.contains(uname)) {
				
				System.out.println("usuari no trobat");
				
			} else if (!passwords.contains(pwd) || (passwords.indexOf(pwd) != users.indexOf(uname))) {
				
				System.out.println("contrassenya incorrecta");
				
			} else {
				
				System.out.println("OK");
			}
			
			
			
		}
		
		//Mostrem la llista d'usuaris ordenada

		for(int i = 0; i < arrayLength; i++) {
			
			//Busquem l'element m�s petit en usuaris
			
			String aux = users.get(0);
			int pos = 0;
			for (int j = 1; j < users.size(); j++) {
				if (users.get(j).compareTo(aux) < 0) {
					aux = users.get(j);
					pos = j;
				}
			}
			//En pos es troba l'usuari alfab�ticament m�s petit, el mostrem i ho esborrem de la llista
			
			
			System.out.print(users.get(pos)+" ");
			users.remove(aux);
			
		}
		System.out.println();
		
		
		sc.close();


	}

}
