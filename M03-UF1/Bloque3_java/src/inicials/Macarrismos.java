package inicials;

import java.util.Scanner;

public class Macarrismos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner (System.in);
		
		final int K = src.nextInt();
		double [] vec;
		vec = new double [K];
		 
		 
		 for (int i= 0; i < K; i++) {
			vec[i] = src.nextDouble();  // Leemos los valores como números decimales.
			
		 }

        for (int i = 0; i < K; i++) {
            // Multiplicamos cada valor por 100 y lo formateamos como porcentaje.
            double porcentaje = vec[i] * 100;
            System.out.printf("%.1f%%", porcentaje);
            if (i < K - 1) {
                System.out.print(" "); // Agregamos un espacio entre los porcentajes, excepto el último.
            }
        }

        src.close(); // Cerramos el Scanner.
    }
}
