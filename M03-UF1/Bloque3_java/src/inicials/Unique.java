package inicials;

import java.util.ArrayList;
import java.util.Scanner;

public class Unique {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner src = new Scanner(System.in);
		
		ArrayList<String> paraules;
		
		for (int casos = src.nextInt(); casos > 0; casos --) {
			
			paraules = new ArrayList<String>();
			int n = src.nextInt();
			src.nextLine();
			for (; n > 0; n--) {
				String pal = src.nextLine();
				if (!paraules.contains(pal)) {
					paraules.add(pal);
				}
			}
			System.out.println(paraules);
		}

	}

}
