package inicials;

import java.util.Scanner;

public class APolisilaba {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		           Scanner scanner = new Scanner(System.in);

		           int casos = scanner.nextInt();
		           scanner.nextLine(); // Consumir la línea en blanco después del número de casos

		           for (int i = 0; i < casos; i++) {
		               String[] palabras = scanner.nextLine().split(" ");

		               // Convertir las palabras a minúsculas para hacer la comparación sin distinguir entre mayúsculas y minúsculas
		               String palabra1 = palabras[0].toLowerCase();
		               String palabra2 = palabras[2].toLowerCase();

		               // Verificar si la frase es una tautología
		               if (palabra1.equals(palabra2)) {
		                   System.out.println("TAUTOLOGIA");
		               } else {
		                   System.out.println("NO TAUTOLOGIA");
		               }
		           }

		           scanner.close();
		       }
	   
	}
