package inicials;

import java.util.Scanner;

public class Trilleros {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		int [] llista = new int[casos];
		
		llista[0] = 1;
		for (int i = 1; i < casos; i++) 
			llista[i] = 0;
			
		int par1 = sc.nextInt();
		int par2 = sc.nextInt();
		
		while (par1 != -1 && par2 != -1) {
			int aux = llista[par1];
			llista[par1] = llista[par2];
			llista[par2] = aux;
			
			par1 = sc.nextInt();
			par2 = sc.nextInt();
		}
			

		//Mostrem el vector modificat
		int pos = 0;
		System.out.print("[");
		for (int i = 0; i < llista.length - 1; i++) {
				System.out.print(llista[i] + ", ");
				if (llista[i] == 1) pos = i;
		}
		if (llista[llista.length-1] == 1) pos = llista.length-1;
		System.out.println(llista[llista.length-1] + "]\n" + pos);

	}

}
