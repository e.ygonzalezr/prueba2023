package inicials;

import java.util.Scanner;

public class NumBinaris {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner src = new Scanner(System.in);
		
		int n1, n2, res;
		String op;
		
		for (int casos=src.nextInt(); casos>0; casos--) {
			
			
			n1=src.nextInt(); 
			op=src.next(); 
			n2=src.nextInt();
			  
			if(op.equals("+"))
				res = n1+n2; 
			else 
				res = n1-n2;
			
			 
		//	System.out.println(Integer.toBinaryString(res));
			
			//Com passem un n�mero decimal a binari?
			String binari = "";
			while (res > 0) {
				binari = binari + (res % 2);
				res = res / 2;
			}
			if (!binari.equals("")) {
				for (int i = binari.length()-1; i >=0; i--)
					System.out.print(binari.charAt(i));
				System.out.println();
			}
			else System.out.println("0");
			
		}
		
		
		src.close();

	}

}
