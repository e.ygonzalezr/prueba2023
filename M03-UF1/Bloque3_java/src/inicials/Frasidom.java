package inicials;

import java.util.Scanner;

public class Frasidom {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner sc = new Scanner(System.in);
		
		String frase = sc.nextLine();
		
		while (!frase.equals(".")) {
			
			boolean frasindrom = true;
			
			String[]palabras = frase.split(" ");
			
			int i = 0;	
			int j = palabras.length-1;
				
			while (i < j && frasindrom) {
					
					if (!palabras[i].equals(palabras[j])) 		
						frasindrom = false;
					i++;
					j--;
					
				}
				
				if (frasindrom) {
					
					System.out.println("SI");
					
				}else {
					
					System.out.println("NO");
					
				}
				
				frase = sc.nextLine();
				
			}
			

	}

}
