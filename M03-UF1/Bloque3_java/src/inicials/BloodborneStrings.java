package inicials;

import java.util.Scanner;

public class BloodborneStrings {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		sc.nextLine();
		
		for (int i = 0; i<casos; i++) {
			
			String palabra = sc.nextLine();
			boolean si = false;
			
			int j = 0;
			while (j<palabra.length()-1 && si == false) {
				if (palabra.charAt(j)==(palabra.charAt(j+1))) {
					si = true;
				}
				j++;
			}
			
			if (si) {
				System.out.println("SI");
			}
			else {
				System.out.println("NO");
			}
		}

	}

}
