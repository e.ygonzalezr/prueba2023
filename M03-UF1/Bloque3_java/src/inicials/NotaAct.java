package inicials;

import java.util.Scanner;

public class NotaAct {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		 
		int nNotes;
		
		for (int casos = sc.nextInt(); casos > 0; casos--) {
			
			nNotes = sc.nextInt();
			int notesAbans[] = new int[nNotes];
			int notesDesp[] = new int[nNotes];
			
			for(int j=0; j<nNotes; j++) {
				notesAbans[j] = sc.nextInt();
			}
			for(int j=0; j<notesDesp.length; j++) {
				notesDesp[j] = sc.nextInt();
			}
			
			int notaMax = 0;
			int cont = 0;
			
			for(int j=0; j<nNotes; j++) {
				int dif = notesDesp[j] - notesAbans[j];
				if(dif > notaMax) {
					cont = 1;
					notaMax = dif;
				}
				else 
					if (dif == notaMax) cont++;
			}
			System.out.println(notaMax + " " + cont);
		}


	}

}
