package inicials;

import java.util.ArrayList;
import java.util.Scanner;

public class ArcaNoe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		  Scanner sc = new Scanner(System.in);

	        int n;
	        while ((n = sc.nextInt()) != 0) {
	            int[] machos = new int[26];
	            int[] hembras = new int[26];

	            for (int i = 0; i < n; i++) {
	                String nombre = sc.next();
	                char genero = nombre.charAt(nombre.length() - 1);
	                int especieIndex = nombre.charAt(0) - 'a';

	                if (genero == 'o') {
	                    machos[especieIndex]++;
	                } else {
	                    hembras[especieIndex]++;
	                }
	            }

	            int parejasCompletas = 0, parejasIncompletas = 0, animalesSobrantes = 0;

	            for (int i = 0; i < 26; i++) {
	                int parejas = Math.min(machos[i], hembras[i]);
	                parejasCompletas += parejas;
	                parejasIncompletas += Math.abs(machos[i] - hembras[i]);
	            }

	            animalesSobrantes = n - 2 * parejasCompletas - parejasIncompletas;

	            System.out.println(parejasCompletas + " " + parejasIncompletas + " " + animalesSobrantes);
	        }

	        sc.close();
	        
	}
	
}