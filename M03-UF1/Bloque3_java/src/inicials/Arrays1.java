package inicials;

import java.util.Scanner;

public class Arrays1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
				Scanner src = new Scanner (System.in);
				
				final int K = src.nextInt();  //Constant que té la grandària del vector
				int [] vec;                   /*declara una variable de tipus vectors d'enters. 
												*El vector encara no esta creat.*/
				vec = new int[K];                //creem un vector d'enters buit de K posicions 
				//omplim el vector amb els nombres que llegim d'entrada
				int i; //index del vector
				
				i=0;
				while(i < K) {
					vec[i] = src.nextInt();
					i++;
			
				}
				//ara el valor d'i es K
				
				//llegim la posicio del vector que volem mostrar, pos, y no i porque la pierdo mas adelante y la necesito
				int pos = src.nextInt(); 
				//mostrem el contingut del vector
				
				i= 0;
				while (i < K) {
					System.out.print(vec[i] + " "); 	
					i++;
				//enseño la casilla y paso la seguent
				}
				
				//mostrar el contingut de la posicio pos
				System.out.println("\n" + vec[pos]);
	
	
	
	
	}
	}


