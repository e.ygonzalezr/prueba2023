package inicials;

import java.util.Scanner;

public class FrancescoVigolini {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner src = new Scanner(System.in);
		
		for (int casos = src.nextInt(); casos > 0; casos--) {
			int k = src.nextInt();
			src.nextLine();
			String [] array = new String [k];
			
			for (int pos =0; pos < k; pos++) {
				array [pos] = src.nextLine();
			}
			int pos = 0;
			boolean trobat = false;
			while (pos <k && trobat == false) {
				if (array [pos].equalsIgnoreCase("Francesco Virgolini")) {
					trobat = true;
				}
				else {
					pos++;
				}
			}
			if (trobat == true) {
				String guard = array [pos-1];
				array [pos-1] = array [pos];
				array [pos] = guard;	
			}
			System.out.print("[");
			for (int p =0; p < k-1; p++) {
				System.out.print(array[p] + ", ");
			}
			System.out.println(array[k-1]+ "]");
		}	

	}

}
