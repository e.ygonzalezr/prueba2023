package inicials;

import java.util.Scanner;

public class ACuantosDiasFaltan {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		 Scanner sc = new Scanner(System.in);

	        // Lee el número de casos de prueba
	        int casos = sc.nextInt();

	        // Vector que contiene la cantidad de días en cada mes
	        int[] diasEnMes = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	        for (int i = 0; i < casos; i++) {
	            // Lee el día y el mes para cada caso
	            int dia = sc.nextInt();
	            int mes = sc.nextInt();

	            // Días totales hasta Nochevieja
	            int diasHastaNochevieja = 0;

	            // Suma los días restantes de cada mes hasta diciembre
	            for (int m = mes; m <= 12; m++) {
	                int diasEnElMes = diasEnMes[m];
	                if (m == mes) {
	                    diasHastaNochevieja += diasEnElMes - dia;
	                } else {
	                    diasHastaNochevieja += diasEnElMes;
	                }
	            }

	            // Imprime el resultado
	            System.out.println(diasHastaNochevieja);
	        }

	        sc.close();
		    }
		
	}

