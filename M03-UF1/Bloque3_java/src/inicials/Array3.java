package inicials;

import java.util.ArrayList;
import java.util.Scanner;

public class Array3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);
		
		ArrayList<Integer> lista = new ArrayList<Integer>(); //Crrem la lista de valors buida
		
		//Omplim la llista de valors
		int valor;
		
		valor = sc.nextInt();
		do {
			lista.add(valor);
			valor = sc.nextInt();
		} while (valor != -1);


		
		int posicio = sc.nextInt(); //Demanem la posició
		
		//Mostrem per pantalla el contingut de l'arrayList
		System.out.print("[");
		for (int i = 0; i < lista.size() -1; i++) 
			System.out.print(lista.get(i) + ", ");
		
		//L'últim valor cal escriure'l fora, perquè és diferent a la resta
		System.out.println(lista.get(lista.size()-1) + "]");
		
		//Mostrar la llista sense necessitat de fer el recorregut pels elements
		System.out.println(lista);
	
		System.out.println(lista.get(posicio)); //Imprimim la posició demanada per l' usuari 
				// La raó es per que comencem a comptar desde 0 en l' array i l' usuari ens demana el 2n valor, per tant en realitat ens demana el tercer

		sc.close();

	}

}
