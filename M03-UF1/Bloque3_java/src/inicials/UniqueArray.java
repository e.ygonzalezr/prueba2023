package inicials;

import java.util.Scanner;

public class UniqueArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner src = new Scanner(System.in);
		
		String [] paraules;
		
		for (int casos = src.nextInt(); casos > 0; casos --) {
		
			int n = src.nextInt();
			src.nextLine();
			
			int posBuida = 0;  //apunta la primera posició buida dins del vector
			paraules = new String[n];
			for (int pos = 0; pos < n; pos++) {
				String pal = src.nextLine();
				//abans de posar la paraula al vector hem de comprovar si ja es troba o no es troba dins del vector. 
				//De la posició 0 fins la primera posició buida	
				
				int i = 0;
				boolean trobat = false;
				while (i < posBuida && trobat == false) {
					if (paraules[i].equals(pal)) {
						trobat = true;
					}
					else {
						i++;
					}
				}
				if (trobat == false) { //pal no es troba dins de paraules
					paraules[posBuida] = pal;
					posBuida++;
				}
			}
			
			System.out.print("[");
			for (int pos = 0; pos < posBuida - 1; pos++) {
				System.out.print(paraules[pos] + ", ");
			}
			System.out.println(paraules[posBuida - 1] + "]");
		}

	}

}
