package inicials;

import java.util.ArrayList;
import java.util.Scanner;

public class IndexOfLlistes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner src = new Scanner(System.in);
			
		for (int casos = src.nextInt(); casos > 0; casos--) {
			int n = src.nextInt();
			ArrayList<Integer> lista = new ArrayList<Integer>();
				
			for(int pos = 0; pos < n; pos++) {
					lista.add(src.nextInt());
			}
			int element = src.nextInt();
			
			
			if(lista.contains(element)) {
				System.out.println(lista.indexOf(element));
			}
		}

	}

}
