package inicials;

import java.util.ArrayList;
import java.util.Scanner;

public class AnimalsPreferitsJordina {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		int casos = sc.nextInt();
		for (int i = 0; i < casos; i++) {
			
			int animals = sc.nextInt();
			sc.nextLine();
			
			ArrayList<String> totAnimals = new ArrayList<String>();
			
			for (int j = 0; j < animals - 1; j++) {
				totAnimals.add(sc.nextLine());
			}
			
			String isAnimal = sc.nextLine();
			
			if(totAnimals.contains(isAnimal)) {
				System.out.println("SI");
			}else {
				System.out.println("NO");
			}
		}

	}

}
