package inicials;

import java.util.Scanner;

public class Comptarem {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		  Scanner src = new Scanner(System.in);  
	        
	        
	        int casos = src.nextInt();  // Leemos un número entero que representa la cantidad de casos.

	        int contador;  // Declaramos una variable para contar las veces que encontramos 'n'.
	        int k;  // Declaramos una variable para la longitud del vector.
	        int n;  // Declaramos una variable para el valor que queremos contar en el vector.
	        int[] vect;  // Declaramos un array (vector) de enteros llamado 'vect'.

	        for (; casos > 0; casos--) {  // Iniciamos un bucle 'for' que se repetirá según el valor de 'casos'.
	            k = src.nextInt();  // Leemos la longitud que queremos para el vector 'vect'.
	            vect = new int[k];  // Creamos un nuevo vector 'vect' con la longitud 'k'.

	            contador = 0;  // Inicializamos el contador a cero.

	            for (int i = 0; i < k; i++) {  // Iniciamos otro bucle 'for' para llenar el vector 'vect' con valores.
	                vect[i] = src.nextInt();  // Leemos un valor y lo almacenamos en el vector 'vect'.
	            }

	            n = src.nextInt();  // Leemos el valor que queremos contar en el vector.

	            for (int i = 0; i < k; i++) {  // Iniciamos otro bucle 'for' para contar cuántas veces aparece 'n' en el vector.
	                if (vect[i] == n) {  // Si el valor en la posición 'i' del vector es igual a 'n'...
	                    contador = contador + 1;  // ...incrementamos el contador en 1.
	                }
	            }

	            System.out.println(contador);  // Imprimimos la cantidad de veces que 'n' aparece en el vector.
	        }
	}

}
