package inicials;

import java.util.ArrayList;
import java.util.Scanner;

public class Jaume {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 Scanner src = new Scanner(System.in); 
		 //recerca d'un element d'una llista, mai es pot servir un for a primeres
		 //per recorrer una llista de paraules, no podem usar un for perquè no podem recorre-la sencera
	
		 
		int casos= src.nextInt();
		src.nextLine();
		
		ArrayList<String> banned = new ArrayList<String>();
		banned.add("hada");
		banned.add("uwu");
		banned.add("sacapuntas");
		banned.add("adolfito");
		banned.add("35");
		
		for(; casos > 0; casos--) {
			
			String frase =src.nextLine().toLowerCase();
			
			String [] paraules = frase.split(" ");
			// cuando separo, split, no necesito atribuirle la grandaria
			int i = 0;
			boolean ban = false;
			
			while (i < paraules.length && !ban) { //!ban: Esto significa que el bucle continuará siempre y cuando la variable booleana ban sea false.
				//SEMPRE QUE HI HA DOS CONDICIONS EN EL WHILE, S'HA DE PREGUNTAR-SE FORA D'ELL QUAN S'ACABI
				if(banned.contains(paraules[i])) ban = true;
				i++;
			} 
			if(ban)	 //true
				 System.out.println("Jaime ha recibido un Ban");
			 
			else 
				System.out.println("No Ban a Jaime");
			
			
		}
		 
		 
		 
	
		
	}

}
