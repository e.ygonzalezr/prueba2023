package inicials;
import java.util.Scanner;

public class Recordar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner (System.in);

		int [] ArrNumeros; //Declarem l' array sense definir l'espai
		int casos = sc.nextInt();
		int tamany;
		int posicio;
		
		for (;casos > 0; casos--) {
			tamany = sc.nextInt(); //Llegim el tamany de l' array 
			ArrNumeros = new int[tamany]; //Inicialitzem l' array amb n nombres que declara l' usuari (tamany)
		
			//Omplim l'array de valors
			for (int i = 0; i < tamany; i++) {	
				ArrNumeros [i] = sc.nextInt(); 
			}
			
			posicio = sc.nextInt(); //Demanem la posició
			
			System.out.println(ArrNumeros [posicio]); //Imprimim la posició demanada per l' usuari 
					// La raó es per que comencem a comptar desde 0 en l' array i l' usuari ens demana el 2n valor, per tant en realitat ens demana el tercer
		}
		sc.close();

		
	}

}
