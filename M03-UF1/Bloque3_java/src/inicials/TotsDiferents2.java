package inicials;

import java.util.ArrayList;
import java.util.Scanner;

public class TotsDiferents2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		int max, i, j;
		ArrayList<Integer> num1;
		int [] num2;
		int valor;
		boolean trobat;
		
		for (;casos > 0; casos--) {
			max = sc.nextInt();
			num1 = new ArrayList<Integer>();
			num2 = new int[max];
		
			//Omplim el vector 1 de valors
			for (i = 0; i < max; i++) num1.add(sc.nextInt());
			
			//Omplim el vector 2 de valors
			for (i = 0; i < max; i++) num2[i] = sc.nextInt();
			
			
			//Recorrem els dos vector2 buscant coincid�ncies per valor. Hem de recorre el vector num2 per cada valor de num1
			trobat = false;
			i = 0; //index per a num1
			do {
				j = 0;  //index per num2
				do {
					if (num1.get(i) == num2[j]) trobat = true;
					j++;
				} while (j < max && !trobat);
				i++;
			} while (i < max && !trobat);
			
			if (trobat) System.out.println("NO");
			else System.out.println("SI");
		}

		sc.close();

	}

}
