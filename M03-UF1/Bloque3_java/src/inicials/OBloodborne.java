package inicials;

import java.util.ArrayList;
import java.util.Scanner;

public class OBloodborne {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner (System.in);
		int casos = src.nextInt();
		
		ArrayList<Integer> lista;
		boolean seguits;
		int total, i;
		
		for (; casos > 0; casos--) {
			
			lista = new ArrayList<Integer>();
			total = src.nextInt();
			
			//omplim amb les dades
			
			for (int x = 0; x < total; x++) {
				lista.add(src.nextInt());
			}
			
			//contar si el siguiente item es igual al anterior
			seguits = false;
			i = 0;
			while ((i < lista.size()-1) && seguits == false) {
				if (lista.get(i) == lista.get(i+1)) {
					seguits = true;
				}
				i++;
			}
			if (seguits == true) {
				System.out.println("SI");
			}
			else  {
				System.out.println("NO");
			}
			
		}

	}

}
