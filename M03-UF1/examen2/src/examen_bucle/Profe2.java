package examen_bucle;

import java.util.Scanner;

public class Profe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);  
		int hora = 0, minut = 0, segon = 0, temps = 0; //temps seria temps total
		boolean correcte = false, fin;
		//Declara dos variables booleanas correcte y fin, inicializándolas en false.
		while (!correcte) {
			System.out.println("Iniciem el rellotge: ");
			System.out.print("Hora: ");
			hora = src.nextInt();  
			if (hora < 0 || hora > 23) correcte = false;
			else {
				System.out.print("Minut: ");
				minut = src.nextInt(); 
				if (minut < 0 || minut > 59) correcte = false;
				else {
					System.out.print("Segon: ");
					segon = src.nextInt();  
					if (segon < 0 || segon > 59) correcte = false;
					else correcte = true;
				}
			}
		} //Sortim del bucle amb el rellotge inicialitzat
		
		do {
			temps = src.nextInt();
			
		} while (temps <= 0);
		
		fin = false;
		while (temps > 0) {
	//	while (!fin) { //while (!fi)
			segon++;
			if (segon == 60) {
				segon = 0;
				minut++;
				if (minut == 60) {
					minut = 0;
					hora++;
					if (hora == 24) {
						hora = 0;
					}
				}
			}
		//System.out.println(hora + ":" + minut + ":" + segon);
		System.out.printf("%02d:%02d:%02d\n", hora, minut, segon);
		temps --;
		}
		src.close();

		
		
	}

}
