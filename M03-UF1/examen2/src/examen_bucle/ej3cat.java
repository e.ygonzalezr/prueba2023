package examen_bucle;

import java.util.Scanner;

public class ej3cat {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner src = new Scanner (System.in);
		
		boolean comprobar_24 = false;
		boolean comprobar_60 = false;
		
		
		/*lo ideal hubiese sido usar el split para separar horas, minutos, segundos. Pero me quedé sin tiempo para testearlo. Lo habria hecho de la manera siguiente:
		 * 
		 * 		String[] horaminseg = input.split(" ");
		 * 		int hora = (int) horasminseg.length(0);
		 * 		int min = (int) horasminseg.length(0);
		 * 		int seg = (int) horasminseg.length(0);
		 * 
		 * */
		
		//revisa la comprobación de el profesor para tener mejor idea, break es mala praxis
		
		
		//inicializamos las varialbes
		int horas = src.nextInt();
		int minutos = src.nextInt();
		int segundos = src.nextInt();
		
		int intervalo = src.nextInt();
		int i = 0;
		
		//aqui hacemos comprobación
		if (horas>=0 && horas<=24) {
			comprobar_24 = true;
		}
		if (minutos<=60 && minutos>=0) {
			comprobar_60 = true;
			if (segundos<=60 && segundos>=0) {
				comprobar_60 = true;
			}
			else {
				comprobar_60 = false;
			}

		}

		
		//comenzamos el bucle, el intervalo es la cantidad de veces que le sumamos 1 al segundo
		while(i<intervalo) {
			
			//se rompe si todas las condiciones son falsas, mala praxis
			if (comprobar_24 == false || comprobar_60 == false) {
				System.out.println("Error, l'hora introduïda no és correcta.");
				break;
			}
			//si por el contrario no, se suma uno al segundo y empezamos a recorrer los condicionales
			segundos++;
			
			//si segundos es mayor a 60, le sumamos uno al minuto y segundos vuelve a 0, así consecutivamente
			if (segundos>60) {
				minutos++;
				segundos = 00;
				if(minutos>60) {
					horas++;
					minutos = 00;
					if(horas>24) {
						horas=00;
					}
				}
			}
			//se printea de la forma adecuada
			System.out.println(horas+" "+minutos+" "+segundos);
			
			
			i++;//proceso para cerrar el bucle con i==intervalo, intervalo es el número de casos.
		}
		//se printea el intervalo.
		if (comprobar_24 == true && comprobar_60 == true) {
			System.out.println(intervalo);
	}
	}
}
