package examen_bucle;

import java.util.Scanner;

public class ej1cat {

	
	//SIN ACABAR !!!!!!!!!!!!!!!!!!!!!!
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner (System.in);
		
		
		//pedimos la string a analizar
		String input = src.nextLine();
		

		
		//incicializamos las variables requeridas

		int i = 0;//contador para iterar bucle
		int contador = 0;//contador de asterisco
		//comprueba si hay un asterisco en el final y el principio de la string
		boolean principio = false;
		boolean finalstring = false;
		char posicion_final = input.charAt(input.length()-1);
		
		//inicializamos la variable char de position para iterar en el bucle
		char position;
		

		/*PROCESO DE COMPROBACIÓN DE VARIABLES
		 * Recorremos cada letra de la string, si encontramos un asterisco aumenta el contador
		 * comprobamos si tenemos un asterisco al principio y al final de la cadena de carácteres
		*/
		//mientras i sea menor a el largo del bucle
		while (i!=input.length()) {
			//le asignamos el valor de la posicion a position
			position = (char) input.charAt(i);
			
			//miramos si la posicion es un asterisco, si lo es aumenta el contador
			if (position == '*') {
				contador++;
				
				//miramos si el asterisco está en el principio o el final, si es así cambian los booleanos
			if (i == 1) {
					principio = true;
				}
			if (i == input.length()-1) {
					finalstring = true;
				}

			}
		i++;
		}
		

		// VALIDAMOS LAS VARIABLES COMPROBADAS EN EL WHILE ANTERIOR
		//si el contador no aumentó, entonces no hay ninguna negreta
		if (contador == 0) {
			System.out.println("No ha fet servir cap negreta");
		}

		//si el contador es igual a dos entonces significa que al menos una parte esta en negreta
		else if (contador == 2) {
			//comprobamos si hay una negreta al principio y otra al final, me falló esta parte del código
			if(principio == true && posicion_final == '*') {
				System.out.println("Tota la frase està en negreta");
			}//si no es true entonces solo alguna parte de la frase está en negreta
			else {
				System.out.println("Alguna part de la frase està en negreta");
			}
			/*falta una condición si el contador es divisible por 2. Si la condición del true es verdad significa que hay al menos cuatro pares de *
			 * por lo que más de una parte de la string tendrá negrita.
			 * esto se hace de la manera siguiente:
			 * if (contador%2 == 0){}
			 * */
		}
		else{
			System.out.println("Negreta no correcte");


	}
	}

}
