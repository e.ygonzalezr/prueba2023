package examen_bucle;

import java.util.Scanner;

public class ej4cat {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner (System.in);
		//pedimos las variables

		int contador = 1;		//contar las veces que se repite el bucle
		
		System.out.print("Indica el número a esbrinar: ");
		int adivinar = src.nextInt();	//pedimos el numero para adivinar
		
		System.out.println(""); 	//espacio en blanco estético
		
		System.out.print("Indica un número: ");		//pedimos el numero al user
		int guess =src.nextInt();
		
		while(guess!=adivinar) {
			//comprobamos si guess es más grande o más pequeño que adivinar, y printea en consecuencia
			if(guess>adivinar) {
				System.out.println("Més petit");
			}
			else {
				System.out.println("Més gran");
			}
			
			contador++;		//sumamos 1 a los intentos
			//repetición de bucle
			System.out.print("Indica un número: ");
			guess =src.nextInt();
		}
		
		System.out.println("FELICITATS, Has esbrintat el número en: "+contador+" vegades");
	}

}
