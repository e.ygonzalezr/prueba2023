package examen_bucle;

import java.util.Scanner;

public class Profe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

Scanner src = new Scanner(System.in);
		
		String frase = src.nextLine();
		int cont = 0;
		int pos = 0;
		
		
		while (pos < frase.length()) {
			if (frase.charAt(pos) == '*') cont ++;
			pos++;
		}
		
		if (cont == 0) {
			System.out.println("No ha fet servir la negreta");
		}
		else {
			if (cont == 2) {
				if (frase.charAt(0) == '*' && frase.charAt(frase.length()-1) == '*') {
					System.out.println("Tota la frase està en negreta");
				}
				else {
					System.out.println("Alguna part de la frase està en negreta");
				}	
			}
			else {
				if (cont > 2 && cont % 2 == 0) {
					System.out.println("Alguna part de la frase està en negreta");
				}
				else {
					System.out.println("Negreta no correcte");
				}
			}
				
		}

		
		
	}

}
