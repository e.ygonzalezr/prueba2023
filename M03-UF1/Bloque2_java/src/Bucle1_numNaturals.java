import java.util.Scanner;

public class Bucle1_numNaturals {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner (System.in);
		System.out.println("Ingrese la cantidad de numeros a verificar:");
	    
		int casos = src.nextInt();
		int contadorNaturals = 0;
		
	
		while(casos >= 1) {
			System.out.println("Ingrese un numero:");
			int elNumero = src.nextInt();
			
			if(elNumero >= 1) {
			contadorNaturals++; 
		
				int suma = elNumero * (elNumero + 1) / 2;
				
				int producto = 1;
					for (int i = 1; i <= elNumero; i++) {
						producto *= i;
					}
             
			System.out.println("SUMA: "+ suma  + " PRODUCTE: " + producto);
			
			}else {
			System.out.println("ELS NOMBRES NATURALS COMENCEN EN 1");
				
			}
			
			casos--;
			
		}
		System.out.println("Cantidad de números naturales ingresados: " + contadorNaturals);
	}

}