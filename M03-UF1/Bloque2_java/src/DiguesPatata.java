import java.util.Scanner;

public class DiguesPatata {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner (System.in);
		int casos; //indica el nombre de casos a tractar. 
		String entrada ;// guarda cada linea;
		
		casos = src.nextInt();
		src.nextLine(); //neteja el buffer d'entrada
		while(casos > 0 ) {
			//comença el tractament d'un cas de prova
			entrada = src.nextLine();
			System.out.println(entrada);
			//fi del cas de prova
			casos = casos -1; // que es lo mismo que "casos--"
			}
		
	}

}
