import java.util.Scanner;

public class Bucles1_ValorMaxMin {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner (System.in);
		int num;
		int max = -9999;
		int min = 9999; //suposem que no posarem numero més grans ni més petits que aquests, és una manera per limitar
		num = src.nextInt();
		//otra forma es poner en esta line: max=num y min=num y la tercera es:
		//min=Integer.Max_Value;
		//max=Integer.Min_Value;
		while(num != 0) {
			//tractar num
			if(num < min ) {
				min =num;
		
			}
			//fi de tractar num
			num=src.nextInt();
		}
		
	}

}
